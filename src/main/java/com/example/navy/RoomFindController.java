package com.example.navy;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class RoomFindController {

    @FXML
    private Button find;

    @FXML
    private Label text;
    @FXML
    public void initialize(){
        find.setOnAction(e -> {
            if (text.getStyle().contains("-fx-text-fill: #0000ff;")){
                text.setStyle("-fx-text-fill: #00ff00;");
                find.setText("Отменить поиск");
                PlayerApplication.findEnemy();
            }else {
                text.setStyle("-fx-text-fill: #0000ff;");
                find.setText("Найти соперника");
                PlayerApplication.stopFindEnemy();
            }
        });
    }
}
