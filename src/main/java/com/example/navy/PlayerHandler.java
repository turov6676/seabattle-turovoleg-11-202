package com.example.navy;

import java.io.*;
import java.net.Socket;

public class PlayerHandler {
    public Socket socket;
    public DataInputStream in;
    public DataOutputStream out;
    public byte queue;
    public PlayerHandler(Socket socket){
        this.socket = socket;
        try {
            this.in = new DataInputStream(socket.getInputStream());
            this.out = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public void writeUTF(String str){
        try {
            out.writeUTF(str);
            out.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
