package com.example.navy;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;

import java.util.ArrayList;

public class BattleFieldController {
    private String [] arr = {"a","b","c","d","e","f","g","h","i","j"};
    @FXML
    public Label queue;
    @FXML
    private Label quantity;
    @FXML
    private GridPane battle_field;
    private ArrayList<Button> buttons = new ArrayList<>();
    @FXML
    public void initialize(String message){
        try {
            queue.setText(message);
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    Button button = new Button();
                    buttons.add(button);
                    button.setId(i + ":" + j);
                    button.setPrefWidth(40);
                    button.setPrefHeight(40);
                    button.setText(arr[j] + i);
                    button.setOnAction(e -> {
                        if (!button.getStyle().contains("-fx-background-color: red") && !button.getStyle().contains("-fx-background-color: grey")) {
                            PlayerApplication.doTurn(button.getId());
                        }
                    });
                    battle_field.add(button, j, i);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    public void getResult(String str){
        String [] arr = str.split(";");
        String res = arr[0];
        String id = arr[1];
        Button button = new Button();
        for (Button b:buttons) {
            if (b.getId().equals(id)){
                button = b;
                break;
            }
        }
        if ("true".equals(res)){
            button.setStyle("-fx-background-color: red");
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    quantity.setText(""+ (Integer.parseInt(quantity.getText()) - 1));
                    if (quantity.getText().equals("0")){
                        PlayerApplication.doTurn("end game");
                    }
                }
            });
        }else if ("false".equals(res)){
            button.setStyle("-fx-background-color: grey");
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    queue.setText("Ход соперника");
                }
            });
        }
    }
}
