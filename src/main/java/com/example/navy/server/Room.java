package com.example.navy.server;

import com.example.navy.Player;
import com.example.navy.PlayerHandler;

import java.io.IOException;

public class Room implements Runnable{
    public boolean online;
    private byte queue;
    public PlayerHandler playerHandler1;
    public PlayerHandler playerHandler2;
    public Player player1 = new Player();
    public Player player2 = new Player();
    public Room(){
        playerHandler1 = null;
        playerHandler2 = null;
        queue = 1;
    }
    public boolean isOnline() {
        return online;
    }

    Thread turnOfFirstPlayer = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                String str;
                while (true) {
                    str = playerHandler1.in.readUTF();
                    if (str.equals("end game")){
                        playerHandler1.writeUTF("end game;Вы победили");
                        playerHandler2.writeUTF("end game;Вы проиграли;");
                    }else {
                        if (queue == 1) {
                            queue = 0;
                            boolean result = player2.isHit(str);
                            if (!result) {
                                queue = 2;
                                playerHandler1.writeUTF("false;" + str);
                                playerHandler2.writeUTF("yours;");
                            } else {
                                queue = 1;
                                playerHandler1.writeUTF("true;" + str);
                            }
                        } else if (str != null) {
                            playerHandler1.writeUTF("not your queue;");
                            Server.print("The next queue is " + queue);
                        }
                    }
                }
            } catch (IOException e) {
                playerHandler1 = null;
            }
        }
    });
    Thread turnOfSecondPlayer = new Thread(new Runnable() {
        @Override
        public void run() {
            String str;
            while (true) {
                try {
                    str = playerHandler2.in.readUTF();
                    if (str.equals("end game")) {
                        playerHandler2.writeUTF("end game;Вы победили");
                        playerHandler1.writeUTF("end game;Вы проиграли;");
                    } else {
                        if (queue == 2) {
                            queue = 0;
                            boolean result = player1.isHit(str);
                            if (!result) {
                                queue = 1;
                                playerHandler2.writeUTF("false;" + str);
                                playerHandler1.writeUTF("yours;");
                            } else {
                                queue = 2;
                                playerHandler2.writeUTF("true;" + str);
                            }
                        } else if (str != null) {
                            playerHandler2.writeUTF("not your queue;");
                        }
                    }
                } catch (IOException e) {
                    playerHandler2 = null;
                }
            }
        }
    });
    @Override
    public void run() {
        turnOfFirstPlayer.start();
        turnOfSecondPlayer.start();
        Server.print("Запуск комнаты");
        playerHandler1.writeUTF("your queue;");
        playerHandler2.writeUTF("enemy queue;");
    }
}
