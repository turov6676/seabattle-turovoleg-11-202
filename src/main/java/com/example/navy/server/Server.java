package com.example.navy.server;

import com.example.navy.PlayerHandler;

import java.io.*;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Server {
    public static int i = 0;
    public static ArrayList<Room> al = new ArrayList<>();
    public static void main(String [] args) throws IOException{
        InetSocketAddress insa = new InetSocketAddress("192.168.0.102", 1000);
        ServerSocket serverSocket = new ServerSocket();
        serverSocket.bind(insa);
        Room r = new Room();
        al.add(r);
        System.out.println("Создание порта сервера");
        while (true) {
            System.out.println("Ожидание подключения");
            Socket socket = serverSocket.accept();
            PlayerHandler playerHandler = new PlayerHandler(socket);
            System.out.println("Подключено");
            createRoomOrAddPlayerHandler(playerHandler);
        }
    }
    public static void closeEverything(Socket socket, BufferedWriter out, BufferedReader in){
        try {
            socket.close();
            out.close();
            in.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void createRoomOrAddPlayerHandler(PlayerHandler playerHandler){
        if (al.get(i).playerHandler1 == null){
            System.out.println("Добавление нового игрока 1 в комнату " + i);
            playerHandler.queue = 1;
            al.get(i).playerHandler1 = playerHandler;
        }else{
            playerHandler.queue = 2;
            al.get(i).playerHandler2 = playerHandler;
            System.out.println("Добавление нового игрока 2 в комнату " + i);
            al.get(i).online = true;
            al.get(i).run();
            al.add(new Room());
            i++;
        }
    }

    public static void print(String str){
        System.out.println(str);
    }
}
