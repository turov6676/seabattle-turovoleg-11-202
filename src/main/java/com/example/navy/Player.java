package com.example.navy;

import com.example.navy.BattleFieldGenerator;
import com.example.navy.server.Server;

public class Player {
    public int [][] battleField = new int[10][10];
    public int masts;
    public BattleFieldGenerator battleFieldGenerator = new BattleFieldGenerator();

    public Player(){
        masts = 20;
        generateRandomField();
        Server.print("Поле создано");
    }
    private void generateRandomField() {
        battleField = battleFieldGenerator.generateShips();
    }

    public void decMast(){
        masts--;
    }
    public boolean isAlive(){
        return masts > 0;
    }
    public boolean isHit(String str){
        int i = Integer.parseInt(str.split(":")[0]);
        int j = Integer.parseInt(str.split(":")[1]);
        if (battleField[i][j] == 1){
            decMast();
        }
        return battleField[i][j] == 1;
    }
}
