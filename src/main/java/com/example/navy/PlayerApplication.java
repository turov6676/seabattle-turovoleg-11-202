package com.example.navy;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
public class PlayerApplication extends Application {
    private static Stage primaryStage;
    private static Socket socket;
    private static DataInputStream in;
    private static DataOutputStream out;
    private static BattleFieldController battleFieldController;
    private static EndGameController endGameController;
    private static RoomFindController roomFindController;
    private static int port = 1000;
    private static String host = "192.168.0.102";
    private static boolean flag = true;
    public static void doTurn(String id) {
        try {
            out.writeUTF(id);
            out.flush();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void print(String str){
        System.out.println(str);
    }
    private static Thread getMessages = new Thread(new Runnable() {
        @Override
        public void run() {
            try {
                String result;
                while (flag) {
                    result = in.readUTF();
                    System.out.println(result);
                    String [] arr = result.split(";");
                    switch (arr[0]){
                        case "end game":
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    toFinishGame(arr[1]);
                                }
                            });
                            break;
                        case "not your queue":
                            break;
                        case "yours":
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    battleFieldController.queue.setText("Ваш ход");
                                }
                            });
                            break;
                        case "true", "false":
                            battleFieldController.getResult(result);
                            break;
                        case "your queue":
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    toStartGame("Ваш ход");
                                }
                            });
                            break;
                        case "enemy queue":
                            Platform.runLater(new Runnable() {
                                @Override
                                public void run() {
                                    toStartGame("Ход соперника");
                                }
                            });
                            break;
                    }
                }
            } catch (IOException e) {
                socket = null;
                in = null;
                out = null;
            }
        }
    });

    public static void toFinishGame(String message) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(PlayerApplication.class.getResource("end-game.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            primaryStage.setScene(scene);
            endGameController = fxmlLoader.getController();
            endGameController.initialize(message);
            primaryStage.show();
            socket.close();
            in.close();
            out.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void toStartGame(String message) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(PlayerApplication.class.getResource("battle-field.fxml"));
            Scene scene = new Scene(fxmlLoader.load(), 600, 400);
            primaryStage.setScene(scene);
            battleFieldController = fxmlLoader.getController();
            battleFieldController.initialize(message);
            primaryStage.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    public static void exit() {
        System.exit(500);
    }

    public static void findEnemy() {
        if (socket == null || socket.isClosed()){
            try {
                socket = new Socket(host,port);
                in = new DataInputStream(socket.getInputStream());
                out = new DataOutputStream(socket.getOutputStream());
                flag = true;
                getMessages.start();
            } catch (IOException e) {
                closeEverything();
            }
        }
    }

    public static void stopFindEnemy() {
        if (socket != null || socket.isClosed()){
            closeEverything();
            socket = null;
            in = null;
            out = null;
            flag = false;
        }
    }

    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(PlayerApplication.class.getResource("rooms.fxml"));
        Scene scene = new Scene(fxmlLoader.load(), 600, 400);
        roomFindController = fxmlLoader.getController();
        stage.setTitle("SeaBattle");
        stage.setScene(scene);
        (primaryStage = stage).show();
    }
    public static void main(String[] args) {
        launch();
    }
    private static void closeEverything(){
        try {
            socket.close();
            in.close();
            out.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}