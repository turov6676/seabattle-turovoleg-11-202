package com.example.navy;

import java.util.Random;

public class BattleFieldGenerator {
    private int [][] battleField;
    private final Random random;
    private int [] shipCount;
    public BattleFieldGenerator(){
        shipCount = new int[]{0,4,3,2,1};
        random = new Random();
        battleField = new int[10][10];
    }
    private void clearBattleField(){
        shipCount = new int[]{0,4,3,2,1};
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                battleField[i][j] = 0;
            }
        }
    }
    public int[][] generateShips() {
        clearBattleField();
        int count = 0;
        for (int i = 1;i < shipCount.length;i++){
            while (shipCount[i] != 0){
                if (count == 100){
                    clearBattleField();
                    count=0;
                    i = 0;
                    break;
                }
                if (addShip(i)){
                    shipCount[i]--;
                }
                count++;
            }
        }
        return battleField;
    }

    private boolean addShip(int shipLen) {
        boolean flag = false;
        if (shipLen == 1) {
            int x = random.nextInt(10);
            int y = random.nextInt(10);
            if (isFree(x, y)) {
                battleField[x][y] = 1;
                return true;
            }else if (isFree(y, x)) {
                battleField[y][x] = 1;
                return true;
            }
        } else {
            int x = random.nextInt(10);
            int y = random.nextInt(10);
            int direction = random.nextInt(2);
            if (x + shipLen - 1 > 9 || y + shipLen - 1 > 9) {
                return false;
            }
            if (direction == 0) {
                for (int i = x; i < x + shipLen; i++) {
                    if (isFree(i, y)) {
                        flag = true;
                    } else {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    for (int i = x; i < x + shipLen; i++) {
                        fillCell(i, y);
                    }
                    return true;
                }
            } else {
                for (int i = y; i < y + shipLen; i++) {
                    if (isFree(x, i)) {
                        flag = true;
                    } else {
                        flag = false;
                        break;
                    }
                }
                if (flag) {
                    for (int i = y; i < y + shipLen; i++) {
                        fillCell(x, i);
                    }
                    return true;
                }
            }
        }
        return flag;
    }
    private void fillCell(int x, int y) {
        int[][] auxiliaryArray = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {1, 1}, {-1, 1}, {1, -1}, {-1, -1}};
        int dx = 0;
        int dy = 0;
        battleField[x][y] = 1;
        for (int i = 0; i < 8; i++) {
            dx = x + auxiliaryArray[i][0];
            dy = y + auxiliaryArray[i][1];
            if ((dx >= 0) && (dx < 10) && (dy >= 0) && (dy < 10) && battleField[dx][dy] != 1) {
                battleField[dx][dy] = 2;
            }
        }
    }

    public boolean isFree(int x, int y) {
        boolean check = false;
        int[][] auxiliaryArray = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {1, 1}, {-1, 1}, {1, -1}, {-1, -1}};
        int dx = 0;
        int dy = 0;
        if ((x >= 0) && (x < 10) && (y >= 0) && (y < 10) && (battleField[x][y] == 0)) {
            int count = 0;
            for (int i = 0; i < 8; i++) {
                dx = x + auxiliaryArray[i][0];
                dy = y + auxiliaryArray[i][1];
                if ((dx >= 0) && (dx < 10) && (dy >= 0) && (dy < 10) && (battleField[dx][dy] == 1)) {
                    count++;
                }
            }
            if (count > 0) {
                check = false;
                return check;
            }else {
                check = true;
            }
        }
        return check;
    }
}
