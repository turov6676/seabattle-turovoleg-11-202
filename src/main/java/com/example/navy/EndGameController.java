package com.example.navy;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class EndGameController {
    @FXML
    private Button exit;
    @FXML
    private Label winner;
    @FXML
    public void initialize(String winner){
        this.winner.setText(winner);
        exit.setOnAction(e -> {
            PlayerApplication.exit();
        });
    }
}

