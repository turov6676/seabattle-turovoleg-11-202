module com.example.navy {
    requires javafx.controls;
    requires javafx.fxml;


    opens com.example.navy to javafx.fxml;
    exports com.example.navy;
    exports com.example.navy.tests;
    opens com.example.navy.tests to javafx.fxml;
    exports com.example.navy.test1;
    opens com.example.navy.test1 to javafx.fxml;
}